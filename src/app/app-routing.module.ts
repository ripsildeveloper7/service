import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InhouseShootComponent } from './model-booking/inhouse-shoot/inhouse-shoot.component';
import { OutDoorshootComponent } from './model-booking/out-doorshoot/out-doorshoot.component';
import { PricingComponent } from './model-booking/pricing/pricing.component';
import { ProductShootComponent } from './model-booking/product-shoot/product-shoot.component';


const routes: Routes = [
  {path : 'FashionCategory' , component : PricingComponent},
  {path : 'ProductShoot' , component : ProductShootComponent},
  {path : 'InHouseShoot' , component : InhouseShootComponent},
  {path : 'OutDoorShoot' , component : OutDoorshootComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
