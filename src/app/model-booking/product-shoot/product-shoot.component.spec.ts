import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductShootComponent } from './product-shoot.component';

describe('ProductShootComponent', () => {
  let component: ProductShootComponent;
  let fixture: ComponentFixture<ProductShootComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductShootComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductShootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
