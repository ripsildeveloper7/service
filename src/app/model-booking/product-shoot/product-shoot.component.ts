import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product-shoot',
  templateUrl: './product-shoot.component.html',
  styleUrls: ['./product-shoot.component.css']
})
export class ProductShootComponent implements OnInit {

shoottype = [
  {'no': 1, 'name' : 'Mannequien'},
  {'no': 2, 'name' : 'Table Top'},
  {'no': 3, 'name' : 'Product Catalogue Shoot'}
]

  constructor() { }

  ngOnInit() {
  }

}
