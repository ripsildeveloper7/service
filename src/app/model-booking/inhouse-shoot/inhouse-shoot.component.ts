import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-inhouse-shoot',
  templateUrl: './inhouse-shoot.component.html',
  styleUrls: ['./inhouse-shoot.component.css']
})
export class InhouseShootComponent implements OnInit {

  description = [
    {'value' : 'Model Cost'},
    {'value' : 'Fashion Photographer'},
    {'value' : 'Studio Cost'},
    {'value' : 'Camera & Equipments'},
    {'value' : 'MakeUp & HairStylist'},
    {'value' : 'Fashion Stylist'},
    {'value' : 'Total Cost'},
    {'value' : 'Post Producion Cost'},
  ]
  constructor() { }

  ngOnInit() {
  }

}
