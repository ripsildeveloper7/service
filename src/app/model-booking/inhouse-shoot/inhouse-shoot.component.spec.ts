import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InhouseShootComponent } from './inhouse-shoot.component';

describe('InhouseShootComponent', () => {
  let component: InhouseShootComponent;
  let fixture: ComponentFixture<InhouseShootComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InhouseShootComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InhouseShootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
