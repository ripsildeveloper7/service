import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pricing',
  templateUrl: './pricing.component.html',
  styleUrls: ['./pricing.component.css']
})
export class PricingComponent implements OnInit {

  productName = [
    {'name' : 'RegularWear'},
    {'name' : 'FormalWear'},
    {'name' : 'EthnicWear'},
    {'name' : 'InnerWear'},
  ]

  productType = [
    {'name' : 'Top'},
    {'name' : 'Bottom'},
    {'name' : 'Set(Top&Bottom)'}, 
       
  ]
  constructor() { }

  ngOnInit() {
  }

}
