import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-out-doorshoot',
  templateUrl: './out-doorshoot.component.html',
  styleUrls: ['./out-doorshoot.component.css']
})
export class OutDoorshootComponent implements OnInit {
  description = [
    {'value' : 'Model Cost'},
    {'value' : 'Fashion Photographer'},
    {'value' : 'Studio Cost'},
    {'value' : 'Camera & Equipments'},
    {'value' : 'MakeUp & HairStylist'},
    {'value' : 'Fashion Stylist'},
    {'value' : 'Total Cost'},
    {'value' : 'Post Producion Cost'},
    {'value' : 'Shoot Location, Travel and Food Cost'},

  ]
  constructor() { }

  ngOnInit() {
  }

}
