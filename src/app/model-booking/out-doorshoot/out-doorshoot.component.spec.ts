import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutDoorshootComponent } from './out-doorshoot.component';

describe('OutDoorshootComponent', () => {
  let component: OutDoorshootComponent;
  let fixture: ComponentFixture<OutDoorshootComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutDoorshootComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutDoorshootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
