import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ModelBookingComponent } from './model-booking/model-booking.component';
import { PricingComponent } from './model-booking/pricing/pricing.component';
import { RouterModule } from '@angular/router';
import { ProductShootComponent } from './model-booking/product-shoot/product-shoot.component';
import { InhouseShootComponent } from './model-booking/inhouse-shoot/inhouse-shoot.component';
import { OutDoorshootComponent } from './model-booking/out-doorshoot/out-doorshoot.component';

@NgModule({
  declarations: [
    AppComponent,
    ModelBookingComponent,
    PricingComponent,
    ProductShootComponent,
    InhouseShootComponent,
    OutDoorshootComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule
  ],

  exports : [PricingComponent,ProductShootComponent,InhouseShootComponent,OutDoorshootComponent],

  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
